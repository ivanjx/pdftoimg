﻿using pdftoimg;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;

namespace pdftoimage.Sample;

internal class Program
{
    static IPopplerService m_popplerService;

    static Program()
    {
        m_popplerService = new PopplerService();
    }

    static async Task Main(string[] args)
    {
        Console.Write("PDF-> ");
        string pdfPath = Console.ReadLine()!.Replace("\"", "");
        string pdfName = Path.GetFileNameWithoutExtension(pdfPath);
        string pdfDir = Path.GetDirectoryName(pdfPath)!;

        Console.Write("Output type (jpg, png)-> ");
        string outputTypeStr = Console.ReadLine()!;
        ImageFormat outputFormat;

        if (outputTypeStr == "jpg")
        {
            outputFormat = ImageFormat.Jpeg;
        }
        else if (outputTypeStr == "png")
        {
            outputFormat = ImageFormat.Png;
        }
        else
        {
            throw new Exception("Invalid output type");
        }

        Console.WriteLine("Reading pdf");
        using Stream pdfStream = File.OpenRead(pdfPath);
        IPopplerDocument document = await m_popplerService.LoadDocumentAsync(pdfStream);
        int pageCount = m_popplerService.GetPagesCount(document);
        IPopplerRenderer renderer = m_popplerService.GetRenderer();

        for (int i = 0; i < pageCount; i++)
        {
            Console.WriteLine("Converting page {0}", i + 1);
            using Bitmap pageBitmap = m_popplerService.RenderPage(
                document,
                renderer,
                i);
            string ext =
                outputFormat == ImageFormat.Jpeg ? "jpg" :
                outputFormat == ImageFormat.Png ? "png" :
                throw new Exception("Invalid format");
            string savePath = Path.Combine(
                pdfDir,
                $"{pdfName}-{i + 1}.{ext}");
            pageBitmap.Save(savePath, outputFormat);
        }

        Console.WriteLine("Done");
    }
}
