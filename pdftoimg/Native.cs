﻿using System;
using System.Runtime.InteropServices;

namespace pdftoimg;

internal static class Native
{
    const string POPPLER_WRAPPER = "PopplerWrapper.dll";

    [DllImport(POPPLER_WRAPPER, CallingConvention = CallingConvention.Cdecl)]
    public static extern nint doc_load(
        byte[] data,
        int dataLen);

    [DllImport(POPPLER_WRAPPER, CallingConvention = CallingConvention.Cdecl)]
    public static extern int doc_get_pages(
        nint doc);

    [DllImport(POPPLER_WRAPPER, CallingConvention = CallingConvention.Cdecl)]
    public static extern void doc_free(
        nint doc);

    [DllImport(POPPLER_WRAPPER, CallingConvention = CallingConvention.Cdecl)]
    public static extern nint renderer_init();

    [DllImport(POPPLER_WRAPPER, CallingConvention = CallingConvention.Cdecl)]
    public static extern nint renderer_render_page(
        nint doc,
        nint renderer,
        int page,
        int dpi);

    [DllImport(POPPLER_WRAPPER, CallingConvention = CallingConvention.Cdecl)]
    public static extern int renderer_free(
        nint renderer);

    [DllImport(POPPLER_WRAPPER, CallingConvention = CallingConvention.Cdecl)]
    public static extern int image_get_width(
        nint image);

    [DllImport(POPPLER_WRAPPER, CallingConvention = CallingConvention.Cdecl)]
    public static extern int image_get_height(
        nint image);

    [DllImport(POPPLER_WRAPPER, CallingConvention = CallingConvention.Cdecl)]
    public static extern void image_copy(
        nint image,
        byte[] data,
        int dataLen);

    [DllImport(POPPLER_WRAPPER, CallingConvention = CallingConvention.Cdecl)]
    public static extern void image_free(
        nint image);
}
