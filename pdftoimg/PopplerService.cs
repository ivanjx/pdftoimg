﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace pdftoimg;

public interface IPopplerService
{
    Task<IPopplerDocument> LoadDocumentAsync(Stream pdfStream);
    int GetPagesCount(IPopplerDocument document);
    IPopplerRenderer GetRenderer();
    Bitmap RenderPage(
        IPopplerDocument document,
        IPopplerRenderer renderer,
        int page,
        int dpi = 150);
}

public class PopplerService : IPopplerService
{
    const int RGB_SIZE = 3;

    public int GetPagesCount(IPopplerDocument document)
    {
        PopplerDocument docImpl = (PopplerDocument)document;
        return Native.doc_get_pages(docImpl.Handle);
    }

    public IPopplerRenderer GetRenderer()
    {
        nint handle = Native.renderer_init();
        return new PopplerRenderer(handle);
    }

    public async Task<IPopplerDocument> LoadDocumentAsync(Stream pdfStream)
    {
        byte[] pdfBuff = new byte[pdfStream.Length];
        int tread = 0;

        while (tread < pdfBuff.Length)
        {
            int read = await pdfStream.ReadAsync(pdfBuff, 0, pdfBuff.Length);
            tread += read;
        }

        nint handle = Native.doc_load(pdfBuff, pdfBuff.Length);

        if (handle == IntPtr.Zero)
        {
            throw new Exception("Error loading pdf document");
        }

        return new PopplerDocument(handle);
    }

    public Bitmap RenderPage(IPopplerDocument document, IPopplerRenderer renderer, int page, int dpi = 150)
    {
        PopplerDocument docImpl = (PopplerDocument)document;
        PopplerRenderer rendererImpl = (PopplerRenderer)renderer;
        nint imageHandle = Native.renderer_render_page(
            docImpl.Handle,
            rendererImpl.Handle,
            page,
            dpi);

        if (imageHandle == IntPtr.Zero)
        {
            throw new Exception("Error rendering page");
        }

        try
        {
            int width = Native.image_get_width(imageHandle);
            int height = Native.image_get_height(imageHandle);
            byte[] buff = new byte[width * height * RGB_SIZE];
            Native.image_copy(
                imageHandle,
                buff,
                buff.Length);
            Bitmap bitmap = new Bitmap(
                width,
                height);

            for (int i = 0, y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    Color color = Color.FromArgb(
                        buff[i],
                        buff[i + 1],
                        buff[i + 2]);
                    bitmap.SetPixel(x, y, color);
                    i += RGB_SIZE;
                }
            }

            return bitmap;
        }
        finally
        {
            Native.image_free(imageHandle);
        }
    }
}
