﻿using System;
using System.Collections.Generic;
using System.Text;

namespace pdftoimg;

public interface IPopplerDocument : IDisposable
{
}

public class PopplerDocument : IPopplerDocument
{
    public nint Handle
    {
        get;
    }

    public PopplerDocument(nint handle)
    {
        Handle = handle;
    }

    public void Dispose()
    {
        Native.doc_free(Handle);
    }
}
