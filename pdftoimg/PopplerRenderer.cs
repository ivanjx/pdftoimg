﻿using System;
using System.Collections.Generic;
using System.Text;

namespace pdftoimg;

public interface IPopplerRenderer : IDisposable
{
}

public class PopplerRenderer : IPopplerRenderer
{
    public nint Handle
    {
        get;
    }

    public PopplerRenderer(nint handle)
    {
        Handle = handle;
    }

    public void Dispose()
    {
        Native.renderer_free(Handle);
    }
}
